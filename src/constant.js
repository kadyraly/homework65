export default {
    pages: [
        {id: 'about', title: 'About'},
        {id: 'contact', title: 'Contact'},
        {id: 'division', title: 'Division'},
        {id: 'place', title: 'Place'},
        {id: 'group', title: 'Group'}
    ]
}