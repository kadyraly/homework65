import React, {Fragment} from 'react';

import './Layout.css';
import {NavLink} from "react-router-dom";

const Layout = props => {
    return (
        <Fragment>
            <nav className="nav">
                <ul>
                    <li><NavLink activeClassName="selected" to="/" exact>Home</NavLink></li>
                    <li><NavLink activeClassName="selected" to="/pages/about">About</NavLink></li>
                    <li><NavLink activeClassName="selected" to="/pages/contact">Contact</NavLink></li>
                    <li><NavLink activeClassName="selected" to="/pages/division">Division</NavLink></li>
                    <li><NavLink activeClassName="selected" to="/pages/place">Place</NavLink></li>
                    <li><NavLink activeClassName="selected" to="/pages/group">Group</NavLink></li>
                    <li><NavLink activeClassName="selected" to="/admin">Admin</NavLink></li>
                </ul>
            </nav>
            <main className="Layout-Content">
                {props.children}
            </main>
        </Fragment>
    )
};

export default Layout;