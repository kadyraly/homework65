import  axios from 'axios';


const instance = axios.create({
    baseURL: 'https://homework65-efe8f.firebaseio.com/'

});

export default instance;