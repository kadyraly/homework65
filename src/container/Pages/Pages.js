import React, {Component} from 'react';
import axios from '../../axios-page';
import './Pages.css';

class Pages extends Component {

    state = {
        info: null,
        loading: false
    };

    componentDidMount() {
        this.getInfo();
    };

    componentDidUpdate (prevProps) {
        if (this.props.match.params.pages !== prevProps.match.params.pages) {
            this.getInfo();
        }
    };

    getInfo = () => {
        axios.get(`/${this.props.match.params.pages}.json`)
            .then(response => {
             this.setState({info: response.data, loading: false})
            })
    };

    render () {
        return this.state.info ?
            <div className="pages">
                <h2>{this.state.info.title}</h2>
                <p>{this.state.info.content}</p>
            </div>
            : null
    };
};

export default Pages;