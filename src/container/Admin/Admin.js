import React, {Component} from 'react';
import constant from "../../constant";
import './Admin.css'
import axios from "../../axios-page";
import Spinner from "../../component/UI/Spinner/Spinner";
class Admin extends Component {

    state = {
            page: constant.pages[0].id,
            title: '',
            content: '',
            loading: false,
            isEdit: false
    };



    componentDidMount() {
        axios.get(`/${constant.pages[0].id}.json`)
            .then(response => response.data ? this.setState({title: response.data.title, content: response.data.content}) : null)
    };

    componentDidUpdate (prevProps, prevState) {
        if (this.state.page !== prevState.page) {
            axios.get(`/${this.state.page}.json`)
                .then(response => response.data ? this.setState({title: response.data.title, content: response.data.content}) : null)
        }
    };

    handleInfoChange = event => {
        event.persist();
        this.setState({[event.target.name]: event.target.value});
    };

    infoSaveHandler = () => {
        this.setState({loading: true});
        const newInfo = {
            title: this.state.title,
            content: this.state.content
        };
        axios.patch(`/${this.state.page}.json`, newInfo)
            .then(() => {
                this.setState({loading: false});
                this.props.history.replace('/pages/' + this.state.page);
            })
    };

    render () {
        return (
            <div className="admin">
                {this.state.loading ? <Spinner /> : null}
                <h3>Pages</h3>
                <select className="select" name="page" onChange={this.handleInfoChange}>
                    {constant.pages.map(page => (
                        <option key={page.id} value={page.id}>{page.title}</option>
                    ))}
                </select>
                <h3>Title</h3>
                <input type="text" name="title" value={this.state.title} onChange={this.handleInfoChange} />
                <h3>Content</h3>
                <textarea name="content" value={this.state.content} onChange={this.handleInfoChange} />
                <button onClick={this.infoSaveHandler}>Save</button>
            </div>
        )
    }

};

export default Admin;