import React, { Component } from 'react';
import {Switch, Route} from "react-router-dom";

import Layout from "./component/Layout/Layout";
import Pages from "./container/Pages/Pages";
import Admin from "./container/Admin/Admin";
import Home from "./component/Home/Home";


class App extends Component {
    render() {
        return (
            <Layout>

                <Switch>
                    <Route path="/" exact component={Home} />
                    <Route path="/admin" component={Admin} />
                    <Route path="/pages/:pages" component={Pages} />
                    <Route render={() => <h1>Not found</h1>} />
                </Switch>
            </Layout>

        );
    }
}

export default App;